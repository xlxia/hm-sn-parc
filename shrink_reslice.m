function shrink_reslice(image_name)
% monkey brain: strink warp
% template (space): MNI152_T1_1mm.nii.gz -> civm_template.nii.gz
addpath(genpath('/usr/local/MATLAB/packages/NIfTI_20140122'));
info=load_untouch_nii(image_name);
%monkey=info.img;
human=info.img;
info=load_untouch_nii('civm_template.nii.gz');
monkey=info.img;

%new=zeros(size(human));
new=zeros(size(monkey));
%new(8:175,12:217,72)=monkey(:,:,60);
new(:,:,60)=human(8:175,12:217,72);
for i=1:59
%    new(8:175,12:217,72-i)=monkey(:,:,60-i);
    new(:,:,60-i)=human(8:175,12:217,72-i);
end
for i=1:(size(human,3)-72)
%    new(8:175,12:217,72+i)=monkey(:,:,60+i);
    new(:,:,60+i)=human(8:175,12:217,72+i);
end

new2=zeros(size(new));
for i=1:size(new,1)
    new2(i,:,:)=new(size(new,1)-i+1,:,:);
end

info.img=new2; info.hdr.dime.cal_max=150; info.hdr.dime.cal_min=0;
save_untouch_nii(info,strcat('shrink_',image_name));
