function magnified_reslice(image_name)
addpath(genpath('/usr/local/MATLAB/packages/NIfTI_20140122'));
info=load_untouch_nii(image_name);
monkey=info.img;
info=load_untouch_nii('/usr/local/fsl/data/standard/MNI152_T1_1mm_brain_float.nii.gz');
human=info.img;

new=zeros(size(human));
new(8:175,12:217,72)=monkey(:,:,60);
for i=1:59
    new(8:175,12:217,72-i)=monkey(:,:,60-i);
end
for i=1:(size(monkey,3)-60)
    new(8:175,12:217,72+i)=monkey(:,:,60+i);
end

new2=zeros(size(new));
for i=1:size(new,1)
    new2(i,:,:)=new(size(new,1)-i+1,:,:);
end

info.img=new2; info.hdr.dime.cal_max=150; info.hdr.dime.cal_min=0;
save_untouch_nii(info,strcat('magnified_',image_name));
